.model small
.code
org 100h
start:
    jmp mulai
nama    db 13,10,'Nama Anda    :$' 
nim     db 13,30,'NIM          :$'
lanjut  db 13,10,'LANJUT tekan y untuk lanjut >>>>>>>>$'
tampung_nama   db 30,?,30 dup(?)
tampung_nim    db 30,?,30 dup(?)    


a db 01   
b db 02
c db 03
d db 04
e db 05
f db 06 
g db 07
j db 15
    

daftar db 13,10,'+-------------------------------------------------+'
       db 13,10,'|                  MOTOR SPORT                    |'
       db 13,10,'+--+----------------------+------+----------------+'
       db 13,10,'|No|Nama                  |Tahun |Harga           |'
       db 13,10,'+--+----------------------+------+----------------+'
       db 13,10,'|01|NINJA H2              |2019  |Rp.760.500.000  |' 
       db 13,10,'+--+----------------------+------+----------------+'
       db 13,10,'|02|BMW S1000RR           |2020  |Rp.767.250.000  |'  
       db 13,10,'+--+----------------------+------+----------------+'
       db 13,10,'|03|CBR1000RR             |2019  |Rp.1.011.000.000|'
       db 13,10,'+--+----------------------+------+----------------+'
       db 13,10,'|04|DUCATI PANIGALE 4     |2019  |Rp.799.390.000  |'
       db 13,10,'+--+----------------------+------+----------------+'
       db 13,10,'|05|MV AGUSTA F4          |2020  |Rp.998.000.000  |'
       db 13,10,'+--+----------------------+------+----------------+'
       db 13,10,'|06|YAMAHA R1M            |2020  |Rp.812.935.000  |'                                          
       db 13,10,'+--+----------------------+------+----------------+'                                          
       db 13,10,'|07|APRILIA RSV4 RF       |2020  |Rp.823.455.000  |'                                          
       db 13,10,'+--+----------------------+------+----------------+'
                                                         
                                                                     
                                                                     
error      db 13,10,'KODE SALAH'
pilih_mtr  db 13,10,'Silahkan Masukkan No/Kode motor yang Anda Pilih :$'
success    db 13,10,'Selamat anda berhasil $'

mulai:
    mov ah,09h
    lea dx,nama
    int 21h  
    mov ah,0ah
    lea dx,tampung_nama  
    int 21h
    push dx  
    
    mov ah,09h
    lea dx,nim
    int 21h  
    mov ah,0ah
    lea dx,tampung_nim  
    int 21h
    push dx  
    
    mov ah,09h
    mov dx,offset daftar
    int 21h
    mov ah,09h
    mov dx,offset lanjut
    int 21h
    mov ah,01h
    int 21h
    cmp al,'y'
    jmp proses
    jne error_msg
    
error_msg:
    mov ah,09h
    mov dx,offset error
    int 21h
    int 20h
    
proses:
    mov ah,09h
    mov dx,offset pilih_mtr
    int 21h
    
    mov ah,1
    int 21h
    mov bh,al
    mov ah,1
    int 21h
    mov bl,al
    cmp bh,'0'
    cmp bl,'1'
    je hasil1
    
    cmp bh,'0'
    cmp bl,'2'
    je hasil2
    
    cmp bh,'0'
    cmp bl,'3'
    je hasil3
    
    cmp bh,'0'
    cmp bl,'4'
    je hasil4
    
    cmp bh,'0'
    cmp bl,'5'
    je hasil5
    
    cmp bh,'0'
    cmp bl,'6'
    je hasil6
    
    cmp bh,'0'
    cmp bl,'7'
    je hasil7   
    
    jne error_msg
    
;--------------------------------------

hasil1:
    mov ah,09h
    lea dx,teks1
    int 21h
    int 20h
    
hasil2:
    mov ah,09h
    lea dx,teks2
    int 21h
    int 20h
    
hasil3:
    mov ah,09h
    lea dx,teks3
    int 21h
    int 20h
    
hasil4:
    mov ah,09h
    lea dx,teks4
    int 21h
    int 20h
    
hasil5:
    mov ah,09h
    lea dx,teks5
    int 21h
    int 20h
    
hasil6:
    mov ah,09h
    lea dx,teks6
    int 21h
    int 20h 
    
hasil7:
    mov ah,09h
    lea dx,teks7
    int 21h
    int 20h 
    
;-------------------------------------

teks1   db 13,10,'Anda memilih motor NINJA H2'
        db 13,10,'Merek KAWASAKI'
        db 13,10,'Tahun 2019'
        db 13,10,'Total harga  : Rp760.500.000'
        db 13,10,'Terima Kasih$'
        
teks2   db 13,10,'Anda memilih motor BMW S1000RR'
        db 13,10,'Merek BMW'
        db 13,10,'Tahun 2020'
        db 13,10,'Total harga  : Rp767.250.000'
        db 13,10,'Terima Kasih$'
        
teks3   db 13,10,'Anda memilih motor CBR1000RR'
        db 13,10,'Merek HONDA'
        db 13,10,'Tahun 2019'
        db 13,10,'Total harga  : Rp1.011.000.000'
        db 13,10,'Terima Kasih$'
        
teks4   db 13,10,'Anda memilih motor DUCATI PANIGALE V4'
        db 13,10,'Merek DUCATI'
        db 13,10,'Tahun 2019'
        db 13,10,'Total harga  : Rp799.390.000'
        db 13,10,'Terima Kasih$'
        
teks5   db 13,10,'Anda memilih motor MV AGUSTA F4'
        db 13,10,'Merek MV AGUSTA'
        db 13,10,'Tahun 2020'
        db 13,10,'Total harga  : Rp998.000.000'
        db 13,10,'Terima Kasih$'
        
teks6   db 13,10,'Anda memilih motor Yamaha R1M'
        db 13,10,'Merek YAMAHA'
        db 13,10,'Tahun 2020'
        db 13,10,'total harga  : Rp812.935.000'
        db 13,10,'Terima Kasih$'  
        
teks7   db 13,10,'Anda memilih motor APRILIA RSV4 RF'
        db 13,10,'Merek APRILIA'
        db 13,10,'Tahun 2020'
        db 13,10,'total harga  : Rp823.455.000'
        db 13,10,'Terima Kasih$' 
        
end start              